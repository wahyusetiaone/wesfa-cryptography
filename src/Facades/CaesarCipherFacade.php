<?php


namespace WESFA\Cryptography;


use Illuminate\Support\Facades\Facade;

class CaesarCipherFacade extends Facade {
    protected static function getFacadeAccessor() { return 'caesarcipher'; }
}
