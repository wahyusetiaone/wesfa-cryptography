<?php

namespace WESFA\Cryptography;
use Illuminate\Support\ServiceProvider;

class CaesarCipherServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('caesarcipher',function(){
            return new CaesarCipher();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
