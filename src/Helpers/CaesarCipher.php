<?php


namespace WESFA\Cryptography;

class CaesarCipher
{
    private static $alphabet = array('A', 'B', 'C', 'D', 'E', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'F');
    private static $alphabet2 = array('8', 'o','b', 'l', 'f', 'e', 'c', 'g', 'd', 'y', 'i', 'x', 'j', 'k', 'u', 'w', '4', 'n', '9', 'm', '7', 'h', '6', 'q', 'p', 'r', 'a', '5', 's', 't', '2', 'v', '3', 'z', '0', '1', '-');
    private static $F15 = 15;
    private static $F13 = 13;
    /**
     * Forward 15 Character.
     *
     * @param string $data
     * @return string
     */
    public static function forward_15(string $data)
    {
        $result = "";
        $split = str_split($data);
        foreach ($split as $char){
            $position =  array_search($char, self::$alphabet,true);
            $position += self::$F15;
            while ($position >= sizeof(self::$alphabet)){
                $position -= sizeof(self::$alphabet);
            }
            $result .= self::$alphabet[$position];
        }
        return $result;
    }
    /**
     * Forward 13 Character.
     *
     * @param string $data
     * @return string
     */
    public static function forward_13(string $data)
    {
        $result = "";
        $split = str_split($data);
        foreach ($split as $char){
            $position =  array_search($char, self::$alphabet2,true);
            $position += self::$F13;
            while ($position >= sizeof(self::$alphabet2)){
                $position -= sizeof(self::$alphabet2);
            }
            $result .= self::$alphabet2[$position];
        }
        return $result;
    }
    /**
     * Backward 13 Character.
     *
     * @param string $data
     * @return string
     */
    public static function backward_13(string $data)
    {
        $result = "";
        $split = str_split($data);
        foreach ($split as $char){
            $position =  array_search($char, self::$alphabet2,true);
            $position -= self::$F13;
            while ($position < 0){
                $position += sizeof(self::$alphabet2);
            }
            $result .= self::$alphabet2[$position];
        }
        return $result;
    }
}
